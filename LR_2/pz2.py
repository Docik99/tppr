from scipy.spatial import distance
from scipy.stats import t
import itertools
from math import sqrt
import numpy as np

def znach (P, n):
    square = sqrt((1 - (P ** 2)) / (n - 2))
    t_interval = t.interval(0.95, df=n - 2)
    if t_interval[0] * square <= P <= t_interval[1] * square:
        res_significance = 'значим'
    else:
        res_significance = 'не значим'
    return res_significance

d1 = np.loadtxt('m1.txt')
d2 = np.loadtxt('m2.txt')
d3 = np.loadtxt('m3.txt')
d4 = np.loadtxt('m4.txt')

sum_mat = (d1+ d2+ d3+ d4)
avg_mat = (d1+ d2+ d3+ d4) / 4
avg_sum_mat = np.sum(sum_mat) / 8

n = 8 #кол-во испытуемых
sa = np.multiply(sum_mat, avg_mat)
s = np.sum(sa)

med = [0]*n
# for i in range(n):
#     med[i]= statistics.median([d1[i],d2[i],d3[i],d4[i]])
val = [1, 2, 3, 4, 5, 6, 7, 8]
perm_set = itertools.permutations(val)
min = 10000
for i in perm_set:
    dist = distance.euclidean(i, d1) + distance.euclidean(i, d2) + distance.euclidean(i, d3) + distance.euclidean(i, d4)
    dist = dist * dist
    if dist < min:
        min = dist
        R = i
print(f"Средняя ранжировка {R}")

min = 10000
for i in perm_set:
    dist = distance.euclidean(i, d1) + distance.euclidean(i, d2) + distance.euclidean(i, d3) + distance.euclidean(i, d4)
    if dist < min:
        min = dist
        R = i
print(f"Медиана {R}")


s1 = np.multiply(d1, avg_mat)
s1_sum = np.sum(s1)
K1 = s1_sum / s
print("Коэф компентности 1 эксперта:",np.around(K1, decimals = 3))

s2 = np.multiply(d2, avg_mat)
s2_sum = np.sum(s2)
K2 = s2_sum / s
print("Коэф компентности 2 эксперта:",np.around(K2, decimals = 3))

s3 = np.multiply(d3, avg_mat)
s3_sum = np.sum(s3)
K3 = s3_sum / s
print("Коэф компентности 3 эксперта:",np.around(K3, decimals = 3))

s4 = np.multiply(d4, avg_mat)
s4_sum = np.sum(s4)
K4 = s4_sum / s
print("Коэф компентности 4 эксперта:",np.around(K4, decimals = 3))


F = [0]*n

vrem = [0]*n
kb_otk = [0]*n
for i in range(n):
    vrem[i] = d1[i]+ d2[i]+ d3[i]+ d4[i]
    #print(vrem[i])
    kb_otk[i] = (vrem[i] - avg_sum_mat)**2
    #print(kb_otk[i])
sums = np.sum(kb_otk)

W = n * sums / ((4**2) * (n**3 - n))
print("коэффициент конкордации:",np.around(W, decimals = 3))


D = [0]*n
for i in range(n):
    D[i] = (d1[i] - d2[i])**2 
DD = np.sum(D)
P = 1- ((6* DD)/ (n * (n**2 - 1)))
print('коэффициент Спирмена между 1 и 2:',np.around(P, decimals = 3), znach(P, n))

for i in range(n):
    D[i] = (d1[i] - d3[i])**2 
DD = np.sum(D)
P = 1- ((6* DD)/ (n * (n**2 - 1)))
print('коэффициент Спирмена между 1 и 3:',np.around(P, decimals = 3), znach(P, n))

for i in range(n):
    D[i] = (d1[i] - d4[i])**2 
DD = np.sum(D)
P = 1- ((6* DD)/ (n * (n**2 - 1)))
print('коэффициент Спирмена между 1 и 4:',np.around(P, decimals = 3), znach(P, n))

for i in range(n):
    D[i] = (d2[i] - d3[i])**2 
DD = np.sum(D)
P = 1- ((6* DD)/ (n * (n**2 - 1)))
print('коэффициент Спирмена между 2 и 3:',np.around(P, decimals = 3), znach(P, n))

for i in range(n):
    D[i] = (d2[i] - d4[i])**2 
DD = np.sum(D)
P = 1- ((6* DD)/ (n * (n**2 - 1)))
print('коэффициент Спирмена между 2 и 4:',np.around(P, decimals = 3), znach(P, n))

for i in range(n):
    D[i] = (d3[i] - d4[i])**2 
DD = np.sum(D)
P = 1- ((6* DD)/ (n * (n**2 - 1)))
print('коэффициент Спирмена между 3 и 4:',np.around(P, decimals = 3), znach(P, n))

