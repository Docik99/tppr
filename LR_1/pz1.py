import numpy as np
import decimal

d0 = np.loadtxt('m0.txt')
d1 = np.loadtxt('m1.txt')
d2 = np.loadtxt('m2.txt')
d3 = np.loadtxt('m3.txt')
wait_mat = (d0 + d1 + d2 + d3) * 2
# получаем матрицу математических ожиданий умноженную на 8

d = np.loadtxt('diff.txt')
diff = d[0]
dec = abs(decimal.Decimal(str(diff).rstrip('0')).as_tuple().exponent)
max_iter = d[1]


def the_math():
    K = [1, 1, 1, 1, 1]  # K0

    # print('Матрица ожиданий:\n', wait_mat)
    res_mat = [1, 1, 1, 1, 1]
    prov = np.array([])
    for k in range(int(max_iter + 1)):
        # Перемножаем на К
        res_mat = np.dot(wait_mat, res_mat)
        # print('Результирующая Матрица:', res_mat)
        # print('k=', k)
        s = np.sum(res_mat)
        # print('Сумма всех элементов:', s)

        for i in range(len(K)):
            K[i] = res_mat[i] / s
        print(f"\nВектор коэффициентов относительной важности объектов порядка {k} : {np.around(K, decimals=dec)}")
        z = 0

        prov = np.append(prov, K)
        # print("Add ", prov)

        for i in range(5):
            if k > 0 and abs(np.around(prov[k * 5 - 5 + i], decimals=dec) - np.around(prov[k * 5 + i], decimals=dec)) < diff:
                z += 1
                if z == 5:
                    print(f"\nПорядок t = {k}")
                    print(f"По мнению экспертов наиболее предпочтителен объект {K.index(np.max(K)) + 1}")
                    return
            elif k == 0:
                break
            else:
                print(f"Дельта {i + 1}: %.{dec}f" % abs(np.around(prov[k * 5 - 5 + i], decimals=dec) - np.around(prov[k * 5 + i], decimals=dec)))
            continue


if __name__ == "__main__":
    the_math()
